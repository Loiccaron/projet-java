package fr.isen.java2.db.daos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.isen.java2.db.entities.Person;

public class PersonDao {
	
	public Person addPerson(Person person) {
		String tmpQuery = "INSERT INTO person(lastname,firstname,nickname, phone_number,address,email_address, birth_date) VALUES(?,?,?,?,?,?,?)";
		try {
			Connection connection = DataSourceFactory.getDataSource().getConnection();
			PreparedStatement statement = connection.prepareStatement(tmpQuery);
			try (PreparedStatement updateName = connection.prepareStatement(tmpQuery)){
				statement.setString(1, person.getLastname());
				statement.setString(2, person.getFirstname());
				statement.setString(3, person.getNickname());
				statement.setString(4, person.getPhone_number());
				statement.setString(5, person.getAddress());
				statement.setString(6, person.getEmail_address());
				statement.setDate(7, Date.valueOf(person.getBirth_date()));
				statement.executeUpdate();
			}
			System.out.println("Add Person output==>  " +person.getLastname()+" "+person.getFirstname()+ " added");
			statement.close();
			connection.close();
			return person;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Person> listPersons() {
		List<Person> listPerson = new ArrayList();
		try {
			Connection connection = DataSourceFactory.getDataSource().getConnection();
			String tmpQuery = "SELECT * FROM person";
			PreparedStatement statement = connection.prepareStatement(tmpQuery);
			try (PreparedStatement updateStatement = connection.prepareStatement(tmpQuery)){
			}
			ResultSet resultSet=statement.executeQuery();
			System.out.println("--------------------------------------------------------------------------------------");
			while (resultSet.next()) {
			  Person person = new Person(resultSet.getInt("idperson"), resultSet.getString("lastname"), resultSet.getString("firstname"), resultSet.getString("nickname"), resultSet.getString("phone_number"), resultSet.getString("address"), resultSet.getString("email_address"),resultSet.getDate("birth_date").toLocalDate());
			  System.out.println("List person output:"+" "+ resultSet.getInt("idperson")+" "+ resultSet.getString("lastname")+" "+ resultSet.getString("firstname")+" "+ resultSet.getString("nickname") + " "+ resultSet.getString("phone_number")+" "+  resultSet.getString("address")+" "+ resultSet.getString("email_address")+" " +resultSet.getDate("birth_date").toLocalDate());
			  listPerson.add(person);
			}
			System.out.println("--------------------------------------------------------------------------------------");
			resultSet.close();
			statement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listPerson;
	}

	public void deletePerson(int i) {
		try {
			Connection connection = DataSourceFactory.getDataSource().getConnection();
			String tmpQuery = "DELETE FROM person WHERE idperson = ?";
			PreparedStatement statement = connection.prepareStatement(tmpQuery);
			try (PreparedStatement updateStatement = connection.prepareStatement(tmpQuery)){
				statement.setInt(1, i);
			}
			statement.executeUpdate();
			statement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void updatePerson(int idToUpdate, Person person) {
		String tmpQuery = "UPDATE person SET lastname= ?, firstname= ?, nickname= ?, phone_number= ?, address =?, email_address = ?, birth_date = ? WHERE idperson = ?";
		try {
			Connection connection = DataSourceFactory.getDataSource().getConnection();
			PreparedStatement statement = connection.prepareStatement(tmpQuery);
			try (PreparedStatement updateName = connection.prepareStatement(tmpQuery)){
				statement.setString(1, person.getLastname());
				statement.setString(2, person.getFirstname());
				statement.setString(3, person.getNickname());
				statement.setString(4, person.getPhone_number());
				statement.setString(5, person.getAddress());
				statement.setString(6, person.getEmail_address());
				statement.setDate(7, Date.valueOf(person.getBirth_date()));
				statement.setInt(8, idToUpdate);
				statement.executeUpdate();
			}
			System.out.println("Update Person output==>  " +person.getLastname()+" "+person.getFirstname()+ " updated");
			statement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}



}
