package fr.isen.java2.db.daos;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.Assertions.tuple;

import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

import fr.isen.java2.db.entities.Person;

public class PersonDaoTestCase {
	private PersonDao personDao = new PersonDao();
	@Before
	public void initDb() throws Exception {
		Connection connection = DataSourceFactory.getDataSource().getConnection();
		Statement stmt = connection.createStatement();
		stmt.executeUpdate(
				"CREATE TABLE IF NOT EXISTS person (idperson INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, lastname VARCHAR(45) NOT NULL,  firstname VARCHAR(45) NOT NULL, nickname VARCHAR(45) NOT NULL, phone_number VARCHAR(15) NULL,address VARCHAR(200) NULL,email_address VARCHAR(150) NULL,birth_date DATE NULL);");
		stmt.executeUpdate("DELETE FROM person");
		stmt.executeUpdate("INSERT INTO person(lastname, firstname, nickname, phone_number, address,email_address, birth_date) "
				+ "VALUES ('Lastname1', 'Firstname1','Nickname1','0606060601','address1','mail1','2015-11-01 12:00:00.000')");
		stmt.executeUpdate("INSERT INTO person(lastname, firstname, nickname, phone_number, address,email_address, birth_date) "
				+ "VALUES ('Lastname2', 'Firstname2','Nickname2','0606060602','address2','mail2','2015-11-02 12:00:00.000')");
		stmt.executeUpdate("INSERT INTO person(lastname, firstname, nickname, phone_number, address,email_address, birth_date) "
				+ "VALUES ('Lastname3', 'Firstname3','Nickname3','0606060603','address3','mail3','2015-11-03 12:00:00.000')");
		stmt.close();
		connection.close();
	}
	
	 @Test
	 public void shouldListPersons() {
			// WHEN
			List<Person> persons = personDao.listPersons();
			// THEN
			assertThat(persons).hasSize(3);
			assertThat(persons).extracting("lastname", "firstname", "nickname", "phone_number", "address","email_address", "birth_date").containsOnly(
					tuple("Lastname1", "Firstname1","Nickname1","0606060601","address1","mail1",LocalDate.of(2015,11,01)),
					tuple("Lastname2", "Firstname2","Nickname2","0606060602","address2","mail2",LocalDate.of(2015,11,02)),
					tuple("Lastname3", "Firstname3","Nickname3","0606060603","address3","mail3",LocalDate.of(2015,11,03)));
	 }
	

	 @Test
	 public void shouldAddPerson() throws Exception {
		 	Person personTest = new Person(8,"Test lastname", "Test Firstname","Test nickname","3630","address test","mail test",LocalDate.of(2015,11,03));
		 	personDao.addPerson(personTest);
			// THEN
			Connection connection = DataSourceFactory.getDataSource().getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM person WHERE lastname='Test lastname'");
			assertThat(resultSet.getString("lastname")).isEqualTo("Test lastname");
			assertThat(resultSet.getString("firstname")).isEqualTo("Test Firstname");
			assertThat(resultSet.getString("nickname")).isEqualTo("Test nickname");
			assertThat(resultSet.getString("phone_number")).isEqualTo("3630");
			assertThat(resultSet.getString("address")).isEqualTo("address test");
			assertThat(resultSet.getString("email_address")).isEqualTo("mail test");
			assertThat(resultSet.getDate("birth_date")).isEqualTo(Date.valueOf(LocalDate.of(2015, 11, 03)));
			List<Person> persons = personDao.listPersons();
			resultSet.close();
			statement.close();
			connection.close();
	 }
	 
	 @Test
	 public void shouldDeletePersons() {
			// WHEN
			List<Person> persons = personDao.listPersons();
			Integer id = persons.get(1).getId();
			personDao.deletePerson(id);
			persons = personDao.listPersons();
			// THEN
			assertThat(persons).hasSize(2);
	 }
	 
	 @Test
	 public void shouldUpdatePersons() {
			// WHEN
			List<Person> persons = personDao.listPersons();
			Integer id = persons.get(0).getId();
			Person personupdate = new Person(8,"update lastname", "update Firstname","update nickname","007","address update","mail@update",LocalDate.of(2015,11,03));
			personDao.updatePerson(id,personupdate);
			persons = personDao.listPersons();
			// THEN
			Person personUpdated =persons.get(0);
			assertThat(personUpdated.getLastname().equals("update lastname"));
			assertThat(personUpdated.getFirstname().equals("update Firstname"));
			assertThat(personUpdated.getNickname().equals("update nickname"));
			assertThat(personUpdated.getPhone_number().equals("007"));
			assertThat(personUpdated.getAddress().equals("address update"));
			assertThat(personUpdated.getEmail_address().equals("mail@update"));
	 }
	
	
	 
	
}
